package com.hivoo.javaWebApp.hello;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {
    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name) {
        String message = "Hello " + name;
        System.out.print(message);

        return message;
    }

    @GetMapping("/")
    public String hello() {
        return "Hello World! ";
    }
}
